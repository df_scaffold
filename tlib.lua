turtle = {}

local mod_prefix = minetest.get_modpath(minetest.get_current_modname())


function turtle.coord(x, y, z)
    return {x = x, y = y, z = z}
end

turtle.pos1 = turtle.coord(0, 0, 0)
turtle.pos2 = turtle.coord(0, 0, 0)

local function format_coord(c)
    return tostring(c.x) .. " " .. tostring(c.y) .. " " .. tostring(c.z)
end

local function parse_coord(c)
end

-- can include ~ + - along with num and ,
local function parse_relative_coord(c)
end

function turtle.ordercoord(c)
    if c.x == nil then
        return {x = c[1], y = c[2], z = c[3]}
    else
        return c
    end
end

-- x or {x,y,z} or {x=x,y=y,z=z}
function turtle.optcoord(x, y, z)
    if y and z then
        return turtle.coord(x, y, z)
    else
        return turtle.ordercoord(x)
    end
end

-- swap x and y if x > y
local function swapg(x, y)
    if x > y then
        return y, x
    else
        return x, y
    end
end

-- swaps coordinates around such that (matching ords of) c1 < c2 and the overall cuboid is the same shape
function turtle.rectify(c1, c2)
    c1.x, c2.x = swapg(c1.x, c2.x)
    c1.y, c2.y = swapg(c1.y, c2.y)
    c1.z, c2.z = swapg(c1.z, c2.z)
    return c1, c2
end

-- converts a coordinate to a system where 0,0 is the southwestern corner of the world
function turtle.zeroidx(c)
    local side = 30912
    return turtle.coord(c.x + side, c.y + side, c.z + side)
end

-- swaps coords and subtracts such that c1 == {0, 0, 0} and c2 is the distance from c1
-- returns rectified c1/c2 and the relativized version
function turtle.relativize(c1, c2)
    c1, c2 = turtle.rectify(c1, c2)

    local c1z = turtle.zeroidx(c1)
    local c2z = turtle.zeroidx(c2)

    local rel = turtle.coord(c2z.x - c1z.x, c2z.y - c1z.y, c2z.z - c1z.z)

    return c1, rel
end

function turtle.cadd(c1, c2)
    return turtle.coord(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z)
end

function turtle.relcoord(x, y, z)
    local pos = minetest.localplayer:get_pos()
    if pos.y > -5000 then pos.y=pos.y-1 end
    return turtle.cadd(pos, turtle.optcoord(x, y, z))
end

local function between(x, y, z) -- x is between y and z (inclusive)
    return y <= x and x <= z
end

function turtle.getdir() --
    local rot = minetest.localplayer:get_yaw() % 360
    if between(rot, 315, 360) or between(rot, 0, 45) then
        return "north"
    elseif between(rot, 135, 225) then
        return "south"
    elseif between(rot, 225, 315) then
        return "east"
    elseif between(rot, 45, 135) then
        return "west"
    end
end
function turtle.setdir(dir) --
    if dir == "north" then
        minetest.localplayer:set_yaw(0)
    elseif dir == "south" then
        minetest.localplayer:set_yaw(180)
    elseif dir == "east" then
        minetest.localplayer:set_yaw(270)
    elseif dir == "west" then
        minetest.localplayer:set_yaw(90)
    end
end

function turtle.dircoord(f, y, r)
    local dir=turtle.getdir()
    local coord = turtle.optcoord(f, y, r)
    local f = coord.x
    local y = coord.y
    local r = coord.z
    local lp=minetest.localplayer:get_pos()
    if dir == "north" then
        return turtle.relcoord(r, y, f)
    elseif dir == "south"  then
        return turtle.relcoord(-r, y, -f)
    elseif dir == "east" then
        return turtle.relcoord(f, y, -r)
    elseif dir== "west" then
        return turtle.relcoord(-f, y, r)
    end

    return turtle.relcoord(0, 0, 0)
end
